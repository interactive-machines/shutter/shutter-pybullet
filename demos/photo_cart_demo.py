#!/usr/bin/env python
"""The demo of Shutter in the photography cart.
"""

import argparse
import numpy as np

import pybullet
import pybullet_data

import _init_paths
from shubu.environments.photo_cart import PhotoCartEnv


def parse_args():
    """Parse arguments.
    Returns:
        args: The parsed arguments.
    """
    parser = argparse.ArgumentParser(description='Basic demo for Shutter in the photography cart.')
    args = parser.parse_args()
    return args


def main():
    args = parse_args()

    env = PhotoCartEnv()
    env.reset_robot_and_scene()

    while(1):

        env.shutter.update()
        env.world.step()


if __name__ == '__main__':
    main()

