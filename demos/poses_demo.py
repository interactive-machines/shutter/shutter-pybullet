#!/usr/bin/env python
"""The demo of Shutter in the simulation.
Shows how to change the pose of the robot by sending position commands to its joints.

joint_1 - base (rotates horizontally)
joint_2 - above base (rotates vertically)
joint_3 - elbow (rotates vertically)
joint_4 - head (rotates vertically)
"""

import argparse
import numpy as np

import pybullet
import pybullet_data

import _init_paths
from shubu.robot.shutter_robot_sim import ShutterRobotSim
from shubu.simulation.world import World


shutter_poses = [
    {
        b'joint_1': 2 * np.pi,
        b'joint_2': 0.0,
        b'joint_3': 0.0,
        b'joint_4': 0.0,
    },
    {
        b'joint_1': 0.0,
        b'joint_2': -0.95,
        b'joint_3': 0.41,
        b'joint_4': -1.0,
    },
    {
        b'joint_1': 0.0,
        b'joint_2': -1.45,
        b'joint_3': 1.45,
        b'joint_4': -1.53,
    }
]


def parse_args():
    """Parse arguments.
    Returns:
        args: The parsed arguments.
    """
    parser = argparse.ArgumentParser(description='Basic demo for a simulated scene with Shutter.')
    args = parser.parse_args()

    return args


def main():
    args = parse_args()

    world = World()
    world.physics.set_additional_search_path(pybullet_data.getDataPath())
    world.reset()

    world.add_body("plane.urdf", is_static=True) # from pybullet_data
    world.start()

    shutter = ShutterRobotSim(world)
    shutter.reset()
    shutter.move_to_joint_positions({
        b'joint_1': 2*np.pi,
        b'joint_2': 0.0,
        b'joint_3': 0.0,
        b'joint_4': 0.0,
        })

    current_pose = -1
    t = 0
    while(1):

        if t % 150 == 0:
            current_pose = (current_pose + 1) % len(shutter_poses)
            shutter.move_to_joint_positions(shutter_poses[current_pose])
            print("Moving to pose {}".format(current_pose))

        shutter.update()
        world.step()
        t += 1

        # print current joint positions
        print(shutter.joint_positions)

        # another way to get the state of a specific joint is shown below, e.g., in the case of join_1
        # joint = shutter._shutter_body.get_joint_by_name(b'joint_1')
        # states = pybullet.getJointStates(joint._uid[0], [joint._uid[1]])
        # position, velocity, reaction_forces, aplied_joint_motor_torque = states[0]


if __name__ == '__main__':
    main()