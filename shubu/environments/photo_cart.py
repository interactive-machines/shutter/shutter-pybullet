import os
import pybullet_data

from shubu.robot.shutter_robot_sim import ShutterRobotSim
from shubu.simulation.world import World


class PhotoCartEnv(object):
    """
    Basic for the robot photographer
    """

    ENV_DATA = os.path.join(os.path.dirname(__file__), 'data')
    CART_URDF = os.path.join(ENV_DATA, 'cart.urdf')

    def __init__(self, use_visualizer=True, time_step=1e-3, key=0, debug=False):

        self.world = World(use_visualizer=use_visualizer,
                           time_step=time_step,
                           key=key,
                           debug=debug)
        self.shutter = None

    def set_additional_search_path(self, path):
        self.world.physics.set_additional_search_path(path)

    def reset_robot_and_scene(self):

        self.world.reset()
        self.set_additional_search_path(pybullet_data.getDataPath())
        self.add_body("plane.urdf", position=[0, 0, -0.8141], is_static=True)  # from pybullet_data
        self.add_body(PhotoCartEnv.CART_URDF, is_static=True)
        self.world.start()


        self.shutter = ShutterRobotSim(self.world, base_position=[-.1, -.16, 0.01])
        self.shutter.reset()

    def add_body(self, path, position=[0, 0, 0], quaternion=[0, 0, 0, 1], scale=1.0, is_static=False):
        body = self.world.add_body(filename=path, position=position, quaternion=quaternion,
                                   scale=scale, is_static=is_static)
        return body

