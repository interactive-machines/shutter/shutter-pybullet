"""Simulated Shutter"""
from PySide2 import QtWidgets, QtGui
import numpy as np
import time

from .shutter_constants import *
from .shutter_robot import ShutterRobot
from .shutter_constants import JOINT_NEUTRAL_POSITIONS
from .robot_command import RobotCommand
from shutter_face.flat_face import FlatFace


class ShutterRobotSim(ShutterRobot):
    """Shutter robot in simulation"""

    def __init__(self,
                 world,
                 base_position=None,
                 base_orientation=None,
                 joint_positions=None,
                 render_face=True):
        """Initialize.
        Args:
            base_position: base position (3d list)
            base_orientation: base orientation as a quaternion
            joint_positions: The list of initial joint positions.
        """
        self._world = world

        # create face
        if render_face:
            self._app = QtWidgets.QApplication()
            self._face = FlatFace(show_window=True, blink=True, face_scale=0.5)
            self._face.hide()
        else:
            self._face = None

        self._base_pose = (base_position, base_orientation)
        self._shutter_body = None

        if joint_positions is None:
            self._initial_joint_positions = JOINT_NEUTRAL_POSITIONS
        else:
            self._initial_joint_positions = joint_positions

        self._world.physics.set_additional_search_path(SHUTTER_DATA)

        self.reboot()

    def reboot(self):
        """Reboot the robot."""
        # Remove the bodies if it already exists.
        if self._shutter_body is not None:
            self._world.remove_body(self._shutter_body)
            self._world.remove_texture(self._face_texture)

        # Add the robot's body
        self._shutter_body = self._world.add_body(
            filename=SHUTTER_URDF,
            position=self._base_pose[0],
            quaternion=self._base_pose[1],
            is_static=True,
            is_controllable=True,
            name='shutter')

        self._shutter_body.set_neutral_joint_positions(JOINT_NEUTRAL_POSITIONS)

        # Find the robot parts.
        self._limb_joints = [
                self._shutter_body.get_joint_by_name(joint_name)
                for joint_name in JOINT_NAMES]
        self._limb_inds = [joint.index for joint in self._limb_joints]

        self._end_effector = self._shutter_body.get_link_by_name(END_EFFCTOR_NAME)

        self._face_plane = self._shutter_body.get_link_by_name(FACE_NAME)
        self._face_texture = self._world.add_texture("tex256.png") # placeholder texture for robot's face
        self._world.physics.change_visual_shape(self._face_plane.uid[0], self._face_plane.uid[1], self._face_texture.uid) # associate face plane with custom texture

        # Reset the positions.
        assert len(self._limb_inds) == len(JOINT_NEUTRAL_POSITIONS)
        assert len(self._limb_inds) == len(self._initial_joint_positions)
        for i, joint_ind in enumerate(self._limb_inds):
            limb_joint = self._shutter_body.joints[joint_ind]
            limb_joint.position = self._initial_joint_positions[i]

    def reset(self, positions=None):
        """Reset the robot.
        """
        # Move the limb to the neural positions or a specified positions.
        if positions is None:
            self.move_to_joint_positions(JOINT_NEUTRAL_POSITIONS)
        else:
            self.move_to_joint_positions(positions)

    def update(self):
        """Update internal robot structures
        """
        if self._face is not None:
            face_image = self._face.renderToImage() # takes about 0.002 sec
            face_array = np.array(face_image.bits(), dtype=np.uint8).copy() # takes about 0.000108 sec
            self._world.physics.change_texture(self._face_texture.uid, face_array, self._face.width(), self._face.height()) # takes about 0.05907 sec

    def move_to_joint_positions(self,
                                positions,
                                speed=MAX_VELOCITY_RATIO,
                                timeout=POSITION_CONTROL_TIMEOUT,
                                threshold=POSITION_THRESHOLD):
        """Move the robot to the specified joint positions.
        See the parent class.
        """
        # Set the maximal joint velocities.
        joint_velocities = dict([
                (joint.name, speed * joint.max_velocity)
                for joint in self._limb_joints
                ])
        kwargs = {
                'joint_velocities': joint_velocities,
                }

        robot_command = RobotCommand(
                component=self._shutter_body.name,
                command_type='set_max_joint_velocities',
                arguments=kwargs)

        self._send_robot_command(robot_command)

        # Command the position control.
        kwargs = {
                'joint_positions': positions,
                'timeout': timeout,
                'threshold': threshold,
                }

        robot_command = RobotCommand(
                component=self._shutter_body.name,
                command_type='set_target_joint_positions',
                arguments=kwargs)

        self._send_robot_command(robot_command)

    def is_ready(self):
        """Check if the model is busy with control commands.
        Returns:
            True if all control commands are done, False otherwise.
        """
        return self._shutter_body.is_ready(joint_inds=self._limb_inds)
     
    def _send_robot_command(self, robot_command):
        """Send a robot command to the server.
        Args:
            robot_command: An instance of RobotCommand.
        """
        self._world.receive_robot_commands(robot_command)

    @property
    def shutter_body(self):
        return self._shutter_body

    @property
    def pose(self):
        return self._shutter_body.pose

    @property
    def joint_positions(self):
        return dict((joint.name, joint.position) for joint in self._limb_joints)

    def change_facial_expression(self, expression_str):
        """
        Change the facial expression of the robot ('neutral', angry', 'bored', 'determined', 'happy', 'happy2', 'sad', 'surprised')
        :param expression_str: string representing the desired facial expression
        """
        self._face.expression_index = expression_str

    def change_pupil_relative_position(self, relx, rely):
        """
        Change the relative position of the pupils
        :param relx: relative position in the horizontal dimension (in [-1,1])
        :param rely: relative position in the vertical dimension (in [-1,1])
        """
        self._face.change_relative_pupil_positions(relx, rely, relx, rely)
