"""A simulated world"""

import numpy as np

from .bullet_physics import BulletPhysics
from .body import Body
from .texture import Texture
from .constraint import Constraint
from ..robot.robot_command import RobotCommand


class World(object):
    """World base class."""

    def __init__(self,
                 use_visualizer=True,
                 time_step=1e-3,
                 gravity=[0, 0, -9.8],
                 key=0,
                 debug=False):
        """Initialize the simulator.
        Args:
            use_visualizer: If use the debugging visualizer duing simulation.
            time_step: Time step of the simulation.
            gravity: The gravity as a 3-dimensional vector.
            key: The key of the simulation. This is used for multi-threading.
            debug: If it is debugging.
        """
        self._debug = debug
        self._gravity = gravity

        # Create the physics backend.
        self._physics = BulletPhysics(time_step=time_step, use_visualizer=use_visualizer, key=key)

    def __del__(self):
        """Delete the world."""
        del self._physics

    def reset(self):
        """Reset the simulation."""
        self.physics.reset()
        self.physics.set_gravity(self._gravity)

        self._bodies = dict()
        self._constraints = dict()
        self._textures = dict()

    def start(self):
        """Start the simulation."""
        self.physics.start()

    def step(self):
        """Take a simulation step."""
        for body in self.bodies.values():
            body.update()

        self.physics.step()

    def add_texture(self,
                    filename,
                    name=None):
        """Load texture in pybullet"""
        texture = Texture(self.physics, filename, name=name)
        self._textures[texture.name] = texture
        return texture

    def add_body(self,
                 filename,
                 position=None,
                 quaternion=None,
                 scale=1.0,
                 is_static=False,
                 is_controllable=False,
                 name=None):
        """Add a body to the simulation.
        Args:
            filename: The path to the URDF or SDF file to be loaded.
            position: A 3 element list with x,y,z position
            quaternion: A 4 element quaternion with x, y, z, w
            is_static: If True, set the base of the body to be static.
            is_controllable: If True, the body can apply motor controls.
            name: Used as a reference of the body in this World instance.
        Returns:
            An instance of Body.
        """
        if position is None:
            position = [0, 0, 0]
        if quaternion is None:
            quaternion = [0, 0, 0, 1]

        # Create the body.
        body = Body(physics=self.physics,
                    filename=filename,
                    position = position,
                    quaternion = quaternion,
                    scale=scale,
                    is_static=is_static,
                    name=name)

        # Add the body to the dictionary.
        self._bodies[body.name] = body

        return body

    def remove_body(self, name):
        """Remove the body.
        Args:
            body: An instance of Body.
        """
        self.physics.remove_body(self._bodies[name].uid)
        del self._bodies[name]

    def add_constraint(self,
                       parent,
                       child,
                       joint_type='fixed',
                       joint_axis=[0, 0, 0],
                       parent_frame_pose=None,
                       child_frame_pose=None,
                       name=None):
        """Add a constraint to the simulation.
        Args:
            parent: The parent entity as an instance of Entity.
            child: The child entity as an instance of Entity.
            joint_type: The type of the joint.
            joint_axis: The axis of the joint.
            parent_frame_pose: The pose of the joint in the parent frame.
            child_frame_pose: The pose of the joint in the child frame.
        Returns:
            An instance of Constraint.
        """
        # Create the constraint.
        constraint = Constraint(
                 parent,
                 child,
                 joint_type,
                 joint_axis,
                 parent_frame_pose,
                 child_frame_pose,
                 name)

        # Add the constraint to the dictionary.
        self._constraints[constraint.name] = constraint

        return constraint

    def receive_robot_commands(self, robot_command: RobotCommand):
        """Receive a robot command.
        Args:
            robot_command: A robot command
        """
        body = self._bodies[robot_command.component]
        command_method = getattr(body, robot_command.command_type)
        command_method(**robot_command.arguments)

    def check_stable(self,
                     body,
                     linear_velocity_threshold,
                     angular_velocity_threshold):
        """Check if the loaded object is stable.
        Args:
            body: An instance of body.
        Returns:
            is_stable: True if the linear velocity and the angular velocity are
            almost zero; False otherwise.
        """
        linear_velocity = np.linalg.norm(body.linear_velocity)
        angular_velocity = np.linalg.norm(body.angular_velocity)

        if linear_velocity_threshold is None:
            has_linear_velocity = False
        else:
            has_linear_velocity = (
                    linear_velocity >= linear_velocity_threshold)

        if angular_velocity_threshold is None:
            has_angular_velocity = False
        else:
            has_angular_velocity = (
                    angular_velocity >= angular_velocity_threshold)

        is_stable = (not has_linear_velocity) and (not has_angular_velocity)

        return is_stable

    def check_contact(self, entity_a, entity_b=None):
        """Check if the loaded object is stable.
        Args:
            entity_a: The first entity.
            entity_b: The second entity, None for any entities.
        Returns:
            True if they have contacts, False otherwise.
        """
        a_uid = entity_a.uid
        if entity_b is None:
            b_uid = None
        else:
            b_uid = entity_b.uid

        contact_points = self._physics.get_contact_points(
                a_uid, b_uid)
        has_contact = len(contact_points) > 0

        return has_contact

    @property
    def physics(self):
        return self._physics

    @property
    def bodies(self):
        return self._bodies

    @property
    def constraints(self):
        return self._constraints