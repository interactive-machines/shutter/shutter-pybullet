Setup Environment for Working with the Simulator in OSX
=======================================================

1. Get Python 3 if you don't have it already. We recommend using Homebrew to install Python 3, as in [this tutorial](https://docs.python-guide.org/starting/install3/osx/).

2. Create a Python 3 [virtual environment](https://docs.python.org/3/library/venv.html) at the root level of this repository to install all dependencies.

   ```bash
   $ cd shutter-pybullet # this repository
   $ python3 -m venv venv # this command will create a directory called 'venv' with the environment
   ```

3. Activate your virtual environment

   ```bash
   $ source venv/bin/activate
   (venv) $ # you should now see the (venv) tag in your terminal
   ```

4. Install shutter-face

    ```bash
   (venv) $ cd submodules
   (venv) $ git submodule init; git submodule update # get shutter-face if the repo was not cloned recursively
   (venv) $ cd shutter-face
   (venv) $ pip3 install .
   (venv) $ cd ../.. # go back to the root of this repository
   ```

5. Install other dependencies within your virtual environment

   ```bash
   (venv) $ pip3 install -r requirements.txt 
   ```

6. Test that the simulation works by running a demo. For example:

   ```bash
   (venv) $ cd demos
   (venv) $ ./poses_demo.py
   ```
   
   the above command should then open up a window that shows the simulated robot. 
   
   <img src="shutter_with_face_pybullet.gif" width="300"/>

When you are done working with the project, exit the environment with the command ```$ deactivate```.