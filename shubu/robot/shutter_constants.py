import os

# Constants
JOINT_NAMES = [
    b'joint_1',
    b'joint_2',
    b'joint_3',
    b'joint_4'
    ]

MAX_JOINT_VELOCITY = {
        b'joint_1': 1.7,
        b'joint_2': 1.7,
        b'joint_3': 1.7,
        b'joint_4': 1.7,
        }

JOINT_NEUTRAL_POSITIONS = [0.00, 0.00, 0.00, 0.00]

# Link name of the end effector, for computing IK.
END_EFFCTOR_NAME = b'head_link'

# Link name of face plane
FACE_NAME = b'face_link'

# Default maximum joint velocity ratio, chosen from (0, 1]
MAX_VELOCITY_RATIO = 1.0

# Default maximum time (in seconds) for position control
POSITION_CONTROL_TIMEOUT = 15.0

# Threshold (in rads) for each joint for position control
POSITION_THRESHOLD = 0.0174533 # 1 degree

SHUTTER_DATA=os.path.join(os.path.dirname(__file__),'models')
SHUTTER_URDF=os.path.join(SHUTTER_DATA,'shutter.v.0.2.urdf')