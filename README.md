Shutter PyBullet
================

Simulation of Shutter in [PyBullet](https://pybullet.org/wordpress/).

Setup
-----

The code is written in Python 3.
Follow the instructions [here](doc/setup_environment_osx.md) 
to setup your environment and install dependencies in OSX.

Dependencies
------------

The code builds on:

- [PyBullet](https://pybullet.org/wordpress/) for simulating the robot. See the [PyBullet Quickstart Guide](https://docs.google.com/document/d/10sXEhzFRSnvFcl3XxNGhnD4N2SedqwdAvK3dsihxVUA/) and [Fabish's shorter guide](https://alexanderfabisch.github.io/pybullet.html).
- [shutter-face](https://gitlab.com/interactive-machines/shutter/shutter-face) to render Shutter's face within PyBullet.

Quick Start
-----------

The core of the code is within the `shubu` Python package, which stands for SHUtter pyBUllet. 
Example usage is provided through demos within the `demos` directory.

### Shutter Demo
An example on how to interface with the robot using the mouse and keyboard is provided in the
`demos/shutter_demo.py` script. To run it:

```bash
$ cd demos
$ ./shutter_demo.py
```

Then you should see the PyBullet GUI come up, with the robot in the middle of the space.
Scroll to zoom in, and option + click (or control + click depending on your OS) to pan the camera view. 

<img src="doc/shutter_demo.png" width="300"/>

Press the keys 0, 1, 2 to change the facial expression of the robot. Use the left, 
right, up, down arrows and the return key to change the direction of the robot's pupils.

### Pose Demo
An example of position control is provided in the `demos/poses_demo.py` script.

```bash
$ cd demos
$ ./poses_demo.py
```

You should then be able to see the robot changing poses in the 
PyBullet GUI:

<img src="doc/shutter_with_face_pybullet.gif" width="280"/>

### Shutter on Photography Cart
An example of Shutter on a simulated version of the photography cart is provided in `demos/photo_cart_demo.py`.

```bash
$ cd demos
$ ./photo_cart_demo.py
```

The script should display the robot, similar to the shutter_demo.py script, but now on the cart:

<img src="doc/shutter_cart.png" width="300"/>

   
Acknowledgements
----------------

The code is inspired by Kuan Fang's [Robovat](https://github.com/kuanfang/robovat/blob/master/robovat) project. Some 
files are modified versions of Kuan's pybullet wrappers. We also rely on C. Gohlke's [transformations.py](https://www.lfd.uci.edu/~gohlke/code/transformations.py.html) script.