#!/usr/bin/env python
"""The demo of Shutter in the simulation.
The program is based on https://github.com/bulletphysics/bullet3/blob/master/examples/pybullet/examples/changeTexture.py
"""

import argparse
import time
import numpy as np

from PySide2 import QtWidgets, QtGui

import pybullet
import pybullet_data

import _init_paths
from shutter_face.flat_face import FlatFace
from shubu.simulation.world import World


def parse_args():
    """Parse arguments.
    Returns:
        args: The parsed arguments.
    """
    parser = argparse.ArgumentParser(description='Basic demo for a simulated scene with Shutter.')
    parser.add_argument('-c', '--opencv2', action='store_true', help="show face in opencv2")
    parser.add_argument('-v', '--verbose', action='store_true', help="print timing info?")
    args = parser.parse_args()
    return args


def main():
    args = parse_args()

    if args.opencv2: # note that using the opencv option to visualize the image creates conflict with pyside at runtime
        import cv2

    # make face widget
    app = QtWidgets.QApplication()
    face = FlatFace(show_window=True, blink=True, face_scale=0.5)

    # make pybullet environment
    world = World()
    world.physics.set_additional_search_path(pybullet_data.getDataPath())
    world.reset()

    # pybullet.setPhysicsEngineParameter(enableFileCaching=0)
    plane_body = world.add_body("plane.urdf", is_static=True) # from pybullet_data
    texUid = pybullet.loadTexture("tex256.png") # note - this texture must have the same dimension as the image used to replace its pixels
    pybullet.changeVisualShape(plane_body.uid, -1, textureUniqueId=texUid)

    world.start()

    pixels = [255] * face.width() * face.height() * 3


    t = 0
    while(1):

        # start measuring time
        duration_loop = time.time()

        keys = pybullet.getKeyboardEvents(world._physics._uid)
        if ord('1') in keys and keys[ord('1')] & pybullet.KEY_WAS_TRIGGERED:
            face.expression_index = 'angry'
        elif ord('2') in keys and keys[ord('2')] & pybullet.KEY_WAS_TRIGGERED:
            face.expression_index = 'sad'
        elif ord('0') in keys and keys[ord('0')] & pybullet.KEY_WAS_TRIGGERED:
            face.expression_index = 'neutral'

        # get face pixels
        face_image = face.renderToImage()  # takes about 0.002 sec
        face_array = np.array(face_image.bits(), dtype=np.uint8).copy()  # takes about 0.000108 sec
        duration_array = time.time() - duration_loop

        # get face pixels one by one... (it's way slower than the code above!)
        # duration_copy = time.time()
        # for i in range(face.width()):
        #     for j in range(face.height()):
        #         r, g, b, a = QtGui.QColor(face.pixel(i, j)).getRgb()
        #
        #         pixels[(i + j * face.width()) * 3 + 0] = r
        #         pixels[(i + j * face.width()) * 3 + 1] = g
        #         pixels[(i + j * face.width()) * 3 + 2] = b
        # duration_copy = time.time() - duration_copy

        duration_change = time.time()
        pybullet.changeTexture(texUid, face_array, face.width(), face.height())
        duration_change = time.time() - duration_change

        if args.opencv2:
            cv_image = np.array(face_array, dtype=np.uint8).reshape(face.height(),face.width(),3)
            cv2.imshow("face", cv_image)
            print("Shape = {}".format(cv_image.shape))
            cv2.waitKey(1)

        # update the world
        world.step()
        t += 1

        duration_loop = time.time() - duration_loop

        if args.verbose:
            print("FPS = {} | Duration array {}, change {}, loop {}".format(1.0/duration_loop, duration_array, duration_change, duration_loop))


if __name__ == '__main__':
    main()