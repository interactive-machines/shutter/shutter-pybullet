"""
Wrapper for Texture
"""
from .base import Base
import os

class Texture(Base):
    """Link of the body."""

    def __init__(self, physics, filename, name=None):
        """Initialize.
        Args:
            physics: pybullet physics instance
            filename: texture filename (have only tested png)
            name: texture name (optional)
        """
        uid = physics.load_texture(filename)

        if name is None:
            model_name, _ = os.path.splitext(os.path.basename(filename))
            name = '%s_%s' % (model_name, uid)

        Base.__init__(self, physics, name, uid)
