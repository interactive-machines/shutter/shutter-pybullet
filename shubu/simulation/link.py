"""The Link class."""

from .base import Base
from ..third_party.transformations import (
    euler_from_quaternion,
    matrix3_from_quaternion
)


class Link(Base):
    """Link of the body."""

    def __init__(self, body, link_ind):
        """Initialize.
        Args:
            body: The body of the joint.
            link_ind: The link index.
        """
        uid = (body.uid, link_ind)
        name = body.physics.get_link_name(uid)
        Base.__init__(self, body.physics, name)
        self._uid = uid
        self._parent = body
        self._index = link_ind

    def set_dynamics(self,
                     mass=None,
                     lateral_friction=None,
                     rolling_friction=None,
                     spinning_friction=None,
                     ):
        """Set dynmamics.
        Args:
            mass: mass
            lateral_friction: lateral friction
            rolling_friction: rolling friction
            spinning_friction: spinning friction
        """
        return self.physics.set_link_dynamics(
                self.uid,
                mass=mass,
                lateral_friction=lateral_friction,
                rolling_friction=rolling_friction,
                spinning_friction=spinning_friction,
                )

    @property
    def parent(self):
        return self._parent

    @property
    def index(self):
        return self._index

    @property
    def pose(self):
        return self.physics.get_link_pose(self.uid)

    @pose.setter
    def pose(self, value):
        raise NotImplementedError

    @property
    def center_of_mass(self):
        return self.physics.get_link_center_of_mass(self.uid)

    @center_of_mass.setter
    def center_of_mass(self, value):
        raise NotImplementedError

    @property
    def mass(self):
        if self._mass is None:
            self._mass = self.physics.get_link_mass(self.uid)
        return self._mass

    @mass.setter
    def mass(self, value):
        raise NotImplementedError

    @property
    def dynamics(self):
        return self.physics.get_link_dynamics(self.uid)

    @property
    def position(self):
        return self.pose[0]

    @position.setter
    def position(self, value):
        raise NotImplementedError

    @property
    def orientation(self):
        return self.pose[1]

    @orientation.setter
    def orientation(self, value):
        raise NotImplementedError

    @property
    def euler(self):
        return euler_from_quaternion(self.orientation)

    @euler.setter
    def euler(self, value):
        raise NotImplementedError

    @property
    def quaternion(self):
        return self.orientation

    @quaternion.setter
    def quaternion(self, value):
        raise NotImplementedError

    @property
    def matrix3(self):
        return matrix3_from_quaternion(self.orientation)

    @matrix3.setter
    def matrix3(self, value):
        raise NotImplementedError
