"""Bullet body object"""

import os.path

import numpy as np

from .base import Base
from .joint import Joint
from .link import Link


class Body(Base):
    """Body."""

    def __init__(self,
                 physics,
                 filename,
                 position,
                 quaternion,
                 scale=1.0,
                 is_static=False,
                 name=None):
        """Initialize."""
        Base.__init__(self, physics, name=name)

        self._uid = self.physics.add_body(filename,
                                          position,
                                          quaternion,
                                          scale=scale,
                                          is_static=is_static)

        self._mass = self.physics.get_body_mass(self.uid)
        self._initial_relative_pose = (position, quaternion)
        self._is_static = is_static

        self._links = [
                Link(self, link_ind)
                for link_ind in self.physics.get_body_link_indices(self.uid)
                ]

        self._joints = [
                Joint(self, joint_ind)
                for joint_ind in self.physics.get_body_joint_indices(self.uid)
                ]

        self._constraints = []

        self.reset_targets()
        self._max_reaction_forces = [None] * len(self.joints)

        if name is None:
            model_name, _ = os.path.splitext(os.path.basename(filename))
            self._name = '%s_%s' % (model_name, self.uid)

    def get_joint_by_name(self, name):
        """Get the joint by the joint name.,
        Args:
            name: The joint name.
        Returns:
            An instance of Joint. Return None if the joint is not found.
        """
        for joint in self.joints:
            if joint.name == name:
                return joint

        raise ValueError('The joint %s is not found in body %s.'
                         % (name, self.name))

    def get_link_by_name(self, name):
        """Get the link by the link name.,
        Args:
            name: The link name.
        Returns:
            An instance of Link. Return None if the link is not found.
        """
        for link in self.links:
            if link.name == name:
                return link

        raise ValueError('The link %s is not found in body %s.'
                         % (name, self.name))

    def set_max_reaction_force(self, joint_ind, force):
        """Set the maximum reaction force for a joint.
        """
        self._max_reaction_forces[joint_ind] = force
        self.joints[joint_ind].enable_sensor()

    def reset_targets(self):
        """Reset the control variables.
        """
        self._max_joint_velocities = [
                joint.max_velocity for joint in self.joints
                ]
        self._target_joint_positions = [None] * len(self.joints)
        self._joint_stop_time = [None] * len(self.joints)
        self._joint_position_threshold = 0.005
        self._joint_velocity_threshold = 0.005
        self._target_link = None
        self._target_link_pose = None
        self._target_link_poses = []
        self._ik_stop_time = None
        self._ik_num_steps = 0

    def is_ready(self, joint_inds=None):
        """Check if the body is ready.
        Args:
            joint_inds: The joints to be checked.
        Returns:
            True if all control commands are done, False otherwise.
        """
        if joint_inds is None:
            if self._target_link is not None:
                return False
            elif any(self._target_joint_positions):
                return False
            else:
                return True
        else:
            is_ready = True

            for joint_ind in joint_inds:
                if self._target_link is not None:
                    if joint_ind < self._target_link.index:
                        is_ready = False

                if self._target_joint_positions[joint_ind] is not None:
                    is_ready = False

            return is_ready

    def set_target_joint_positions(self,
                                   joint_positions,
                                   timeout=15,
                                   threshold=0.005):
        """Set target joint positions for position control.
        Args:
            joint_positions: The joint positions for each specified joint.
            timeout: Seconds to wait for move to finish.
            threshold: Joint position threshold in radians across each joint
                when move is considered successful.
        """
        stop_time = self.physics.time() + timeout
        if isinstance(joint_positions, (list, tuple)):
            for joint_ind, joint_position in enumerate(joint_positions):
                if joint_position is not None:
                    self._target_joint_positions[joint_ind] = joint_position
                    self._joint_stop_time[joint_ind] = stop_time
        elif isinstance(joint_positions, dict):
            for key, joint_position in joint_positions.items():
                joint = self.get_joint_by_name(key)
                joint_ind = joint.index
                self._target_joint_positions[joint_ind] = joint_position
                self._joint_stop_time[joint_ind] = stop_time
        else:
            raise ValueError

    def set_target_link_pose(self,
                             link_ind,
                             link_pose,
                             timeout=15,
                             threshold=0.005):
        """Set the target pose of the link for position control.
        Args:
            link_ind: The index of the end effector link.
            link_pose: The pose of the end effector link.
            timeout: Seconds to wait for move to finish.
            threshold: Joint position threshold in radians across each joint
                when move is considered successful.
        """
        self._ik_stop_time = self.physics.time() + timeout
        self._target_link = self.links[link_ind]
        self._target_link_pose = link_pose
        self._ik_num_steps = 0

    def set_target_link_poses(self,
                              link_ind,
                              link_poses,
                              timeout=15,
                              threshold=0.005):
        """Set the target poses of the link for position control.
        The list of poses should be reached sequentially.
        Args:
            link_ind: The index of the end effector link.
            link_pose: The pose of the end effector link.
            timeout: Seconds to wait for move to finish.
            threshold: Joint position threshold in radians across each joint
                when move is considered successful.
        """
        assert isinstance(link_poses, list)
        self._ik_stop_time = self.physics.time() + timeout
        self._target_link = self.links[link_ind]
        self._target_link_poses = link_poses
        self._joint_position_threshold = threshold

    def set_neutral_joint_positions(self, joint_positions):
        """Set the neutral joint positions.
        This is used for computing the IK solution.
        Args:
            joint_positions: A list of joint positions.
        """
        self._neutral_joint_positions = joint_positions

    def set_max_joint_velocities(self, joint_velocities):
        """Set the maximal joint velocities for position control.
        Args:
            joint_velocities: The maximal joint velocities.
        """
        if isinstance(joint_velocities, (list, tuple)):
            for joint_ind, joint_velocity in enumerate(joint_velocities):
                if joint_velocity is not None:
                    self._max_joint_velocities[joint_ind] = joint_velocity
        elif isinstance(joint_velocities, dict):
            for key, joint_velocity in joint_velocities.items():
                joint = self.get_joint_by_name(key)
                joint_ind = joint.index
                self._max_joint_velocities[joint_ind] = joint_velocity
        else:
            raise ValueError

    def update(self):
        """Update control and disturbances."""
        if self._target_link is not None:
            is_ik_done = self._update_ik()
            if is_ik_done:
                self._ik_num_steps = 0
            else:
                self._ik_num_steps += 1
        self._update_position_control()

    def _update_position_control(self):
        """Update the position control."""
        if not any(self._target_joint_positions):
            return
        target_joint_inds = []
        target_joint_positions = []
        target_joint_velocities = []
        for joint_ind, joint in enumerate(self.joints):
            target_joint_position = self._target_joint_positions[joint_ind]
            if target_joint_position is not None:
                target_joint_inds.append(joint_ind)
                target_joint_positions.append(target_joint_position)
                target_joint_velocities.append(0)

        assert len(target_joint_inds) == len(target_joint_positions)
        assert len(target_joint_inds) == len(target_joint_velocities)

        for i, joint_ind in enumerate(target_joint_inds):
            joint = self.joints[joint_ind]
            self.physics.position_control(
                    joint.uid,
                    target_position=target_joint_positions[i],
                    target_velocity=target_joint_velocities[i],
                    position_gain=0.05,
                    velocity_gain=1.0)

        is_reached = self.position_reached(target_joint_inds,
                                           target_joint_positions,
                                           target_joint_velocities,
                                           self._joint_position_threshold,
                                           self._joint_velocity_threshold)
        current_time = self.physics.time()
        for joint_ind in target_joint_inds:
            is_timeout = self.check_joint_timeout(joint_ind, current_time)
            max_force = self._max_reaction_forces[joint_ind]
            if max_force is None:
                is_safe = True
            else:
                joint = self.joints[joint_ind]
                reaction_force = joint.reaction_force
                reaction_force_norm = np.linalg.norm(reaction_force[:3])
                is_safe = reaction_force_norm < max_force
            if is_reached or is_timeout or (not is_safe):
                self._target_joint_positions[joint_ind] = None
                self._joint_stop_time[joint_ind] = None

    def _update_ik(self):
        """Update the inverse kinematics results.
        Returns:
            done: If the IK is done.
        """
        ik_link_ind = self._target_link.index
        joint_inds = range(ik_link_ind)
        if self._target_link_pose is None:
            if len(self._target_link_poses) == 0:
                raise ValueError
            else:
                self._target_link_pose = self._target_link_poses[0]
                self._target_link_poses = self._target_link_poses[1:]

                for joint_ind in joint_inds:
                    self._target_joint_positions[joint_ind] = None

        joint_positions = self.physics.compute_inverse_kinematics(
                self._target_link.uid,
                self._target_link_pose,
                upper_limits=self.joint_upper_limits,
                lower_limits=self.joint_lower_limits,
                ranges=self.joint_ranges,
                damping=[0.7] * len(self.joints),
                neutral_positions=self._neutral_joint_positions)

        if len(self._target_link_poses) == 0:
            joint_velocities = [0] * len(joint_inds)
        else:
            joint_velocities = [None] * len(joint_inds)
        is_converged = self.position_reached(joint_inds,
                                             joint_positions,
                                             joint_velocities,
                                             self._joint_position_threshold,
                                             self._joint_velocity_threshold)
        is_timeout = self.physics.time() >= self._ik_stop_time
        if is_converged or is_timeout:
            self._target_link_pose = None
            if len(self._target_link_poses) == 0:
                self._target_link = None
                self._ik_stop_time = None
            return True
        else:
            for joint_ind, joint_position in zip(joint_inds, joint_positions):
                self._target_joint_positions[joint_ind] = joint_position
                self._joint_stop_time[joint_ind] = self._ik_stop_time
            return False

    def position_reached(self, joint_inds, joint_positions, joint_velocities,
                         joint_position_threshold, joint_velocity_threshold):
        """Check if the specified joint positions are reached.
        Args:
            joint_inds: List of joint indices.
            joint_positions: List of target joint positions.
            joint_positions: List of target joint velocities.
        Returns:
            True or False.
        """
        for joint_ind, target_position, target_velocity in zip(
                joint_inds, joint_positions, joint_velocities):
            current_position = self.joints[joint_ind].position
            delta_position = target_position - current_position
            position_reached = (
                    abs(delta_position) < joint_position_threshold)
            if target_velocity is None:
                velocity_reached = True
            else:
                current_velocity = self.joint_velocities[joint_ind]
                delta_velocity = target_velocity - current_velocity
                velocity_reached = (
                        abs(delta_velocity) < joint_velocity_threshold)
            if not (position_reached and velocity_reached):
                return False
        return True

    def check_joint_timeout(self, joint_ind, current_time=None):
        """Check if the joint is timeout.
        Args:
            joint_ind: The joint index.
            current_time: The current simulation time.
        Returns:
            True for timeout, False otherwise.
        """
        if current_time is None:
            current_time = self.physics.time()

        is_timeout = current_time >= self._joint_stop_time[joint_ind]

        return is_timeout

    def check_joint_safe(self, joint_ind):
        """Check if the joint is safe.
        Args:
            joint_ind: The joint index.
        Returns:
            True for safe, False otherwise.
        """
        max_force = self._max_reaction_forces[joint_ind]

        if max_force is None:
            return True
        else:
            joint = self.joints[joint_ind]
            reaction_force = joint.reaction_force
            reaction_force_norm = np.linalg.norm(reaction_force[:3])
            is_safe = reaction_force_norm < max_force
            return is_safe

    @property
    def dynamics(self):
        return self.physics.get_body_dynamics(self.uid)

    @property
    def is_static(self):
        return self._is_static

    @property
    def contacts(self):
        return self.physics.get_body_contacts(self.uid)

    @property
    def links(self):
        return self._links

    @property
    def joints(self):
        return self._joints

    @property
    def pose(self):
        return self.physics.get_body_pose(self.uid)

    @property
    def position(self):
        return self.pose[0]

    @property
    def orientation(self):
        return self.pose[1]

    @property
    def joint_positions(self):
        return [joint.position for joint in self.joints]

    @property
    def joint_velocities(self):
        return [joint.velocity for joint in self.joints]

    @property
    def joint_lower_limits(self):
        return [joint.lower_limit for joint in self.joints]

    @property
    def joint_upper_limits(self):
        return [joint.upper_limit for joint in self.joints]

    @property
    def joint_max_efforts(self):
        return [joint.max_effort for joint in self.joints]

    @property
    def joint_max_velocities(self):
        return [joint.max_velocity for joint in self.joints]

    @property
    def joint_dampings(self):
        return [joint.damping for joint in self.joints]

    @property
    def joint_frictions(self):
        return [joint.friction for joint in self.joints]

    @property
    def joint_ranges(self):
        return [joint.range for joint in self.joints]

    @property
    def linear_velocity(self):
        return self.physics.get_body_linear_velocity(self.uid)

    @property
    def angular_velocity(self):
        return self.physics.get_body_angular_velocity(self.uid)

    @property
    def mass(self):
        return self._mass

    @pose.setter
    def pose(self, value):
        self.physics.set_body_pose(self.uid, value)

    @position.setter
    def position(self, value):
        self.physics.set_body_position(self.uid, value)

    @orientation.setter
    def orientation(self, value):
        self.physics.set_body_orientation(self.uid, value)

    @joint_positions.setter
    def joint_positions(self, value):
        for joint, joint_position in zip(self.joints, value):
            joint.position = joint_position

    @linear_velocity.setter
    def linear_velocity(self, value):
        self.physics.set_body_linear_velocity(self.uid, value)

    @angular_velocity.setter
    def angular_velocity(self, value):
        self.physics.set_body_angular_velocity(self.uid, value)

    @mass.setter
    def mass(self, value):
        self.physics.set_body_mass(self.uid, value)