"""The basic class of robots."""
from __future__ import annotations

import abc
from .shutter_constants import *

# Main Shutter Robot Class
class ShutterRobot(object):
    """Base class for Shutter robot."""
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def reset(self, positions=None):
        """Reset the robot.
        If the positions are not specified, the robot will go to the neutral
        positions.
        Args:
            positions: The target joint positions, as a list or a dictionary of
                joint_name:joint_position.
        """
        raise NotImplementedError


    @abc.abstractmethod
    def update(self):
        """Update internal robot structures
        """
        raise NotImplementedError

    @abc.abstractmethod
    def move_to_joint_positions(self,
                                positions,
                                speed=MAX_VELOCITY_RATIO,
                                timeout=POSITION_CONTROL_TIMEOUT,
                                threshold=POSITION_THRESHOLD):
        """Move the robot to the specified joint positions.
        Args:
            positions: The target joint positions, as a dictionary of
                joint_name:joint_position.
            speed: The maximum joint velocity.
            timeout: Seconds to wait for move to finish.
            threshold: Joint position threshold in radians across each joint
                when move is considered successful.
        """
        raise NotImplementedError

    @property
    def pose(self):
        raise NotImplementedError

    @property
    def position(self):
        return self.pose.position

    @property
    def orientation(self):
        return self.pose.orientation

    @property
    def euler(self):
        return self.orientation.euler

    @property
    def quaternion(self):
        return self.orientation.quaternion

    @property
    def matrix3(self):
        return self.orientation.matrix3

    @abc.abstractmethod
    def change_facial_expression(self, expression_str):
        """
        Change the facial expression of the robot ('neutral', angry', 'bored', 'determined', 'happy', 'happy2', 'sad', 'surprised')
        :param expression_str: string representing the desired facial expression
        """
        raise NotImplementedError

    @abc.abstractmethod
    def change_pupil_relative_position(self, relx, rely):
        """
        Change the relative position of the pupils
        :param relx: relative position in the horizontal dimension (in [-1,1])
        :param rely: relative position in the vertical dimension (in [-1,1])
        """
        raise NotImplementedError

    def look_up(self):
        """
        Make the robot look up
        """
        self.change_pupil_relative_position(0, -1)

    def look_down(self):
        """
        Make the robot look down
        """
        self.change_pupil_relative_position(0, 1)

    def look_right(self):
        """
        Make the robot look to its right
        """
        self.change_pupil_relative_position(-1, 0)

    def look_left(self):
        """
        Make the robot look to its right
        """
        self.change_pupil_relative_position(1, 0)

    def look_forward(self):
        """
        Make the robot look to its right
        """
        self.change_pupil_relative_position(0, 0)