"""Base simulation element class"""

class Base(object):
    """The base class for simulation elements."""

    def __init__(self, physics, name=None, uid=None):
        """Initialize.
        Args:
            physics: The physics of the base.
            name: The name of the base.
            uid: id of the simulation lement
        """
        self._physics = physics

        self._uid = uid
        self._name = name

    @property
    def physics(self):
        return self._physics

    @property
    def uid(self):
        return self._uid

    @property
    def name(self):
        return self._name
