#!/usr/bin/env python
"""The demo of Shutter in the simulation.

Click on the robot to change its pose. Use the keys below to change the face expression:
1 - show angry expression
2 - show sad expression
0 - show nuetral expression (default)

Or change its gaze direction:
left_arrow - make the robot look towards its left
right_arrow - make the robot look towards its right
up_arrow - make the robot look up
down_arrow - make the robot look down
return - make the robot look forward (default)

You can zoom towards the robot using the wheel on the mouse or scrolling gesture. Pressing option and clicking
pans the camera in the PyBullet GUI.
"""

import argparse
import time
import numpy

import pybullet_data
import pybullet

import _init_paths
from shubu.robot.shutter_robot_sim import ShutterRobotSim
from shubu.simulation.world import World




def parse_args():
    """Parse arguments.
    Returns:
        args: The parsed arguments.
    """
    parser = argparse.ArgumentParser(description='Basic demo for a simulated scene with Shutter.')
    args = parser.parse_args()

    return args


def main():
    args = parse_args()

    world = World(time_step=1e-3, debug=True)
    world.physics.set_additional_search_path(pybullet_data.getDataPath())
    world.reset()

    world.add_body("plane.urdf", is_static=True) # from pybullet_data
    world.start()

    shutter = ShutterRobotSim(world)
    shutter.reset()

    t = 0
    fps = []
    while(1):

        start_time = time.time()

        shutter.update()
        world.step()
        keys = pybullet.getKeyboardEvents(world._physics._uid)
        if ord('1') in keys and keys[ord('1')]&pybullet.KEY_WAS_TRIGGERED:
            shutter.change_facial_expression('angry')
        elif ord('2') in keys and keys[ord('2')]&pybullet.KEY_WAS_TRIGGERED:
            shutter.change_facial_expression('sad')
        elif ord('0') in keys and keys[ord('0')]&pybullet.KEY_WAS_TRIGGERED:
            shutter.change_facial_expression('neutral')
        elif pybullet.B3G_RIGHT_ARROW in keys and keys[pybullet.B3G_RIGHT_ARROW]&pybullet.KEY_WAS_TRIGGERED:
            shutter.look_right()
        elif pybullet.B3G_LEFT_ARROW in keys and keys[pybullet.B3G_LEFT_ARROW]&pybullet.KEY_WAS_TRIGGERED:
            shutter.look_left()
        elif pybullet.B3G_UP_ARROW in keys and keys[pybullet.B3G_UP_ARROW]&pybullet.KEY_WAS_TRIGGERED:
            shutter.look_up()
        elif pybullet.B3G_DOWN_ARROW in keys and keys[pybullet.B3G_DOWN_ARROW]&pybullet.KEY_WAS_TRIGGERED:
            shutter.look_down()
        elif pybullet.B3G_RETURN in keys and keys[pybullet.B3G_RETURN]&pybullet.KEY_WAS_TRIGGERED:
            shutter.look_forward()

        tmp = 1.0 / (time.time() - start_time)
        fps.append(tmp)
        if (len(fps) > 50):
            fps.pop(0)
        t += 1

        if t % 10 == 0:
            print("\rFPS: %.3f" % numpy.mean(fps), end='')


if __name__ == '__main__':
    main()